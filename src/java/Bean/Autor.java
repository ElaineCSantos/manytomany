/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

/**
 *
 * @author ElaineSantos
 * classe que representa o objeto autor dentro da aplicação 
 */
@Entity(name="autor")
public class Autor {
    
    @Id //define que o atributo id será a chave da tabela
    @Column(name="id_autor") // o atributo id estara na coluna "id_autor" no BD
    @GeneratedValue // id incrementa automatico
    private int id;
    
    @Column(name="nome_autor", nullable=false, columnDefinition="VARCHAR(100)", length=100)
    //a linha acima define que no banco o atributo nome estara na coluna "nome_autor", não pode ser nulo, é definido como varchar e seu tamanho é 100.
    private String nome;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "livro_autor", 
        joinColumns = { @JoinColumn(name = "id_autor") }, 
        inverseJoinColumns = { @JoinColumn(name = "isbn") })
    private List<Livro> livros = new ArrayList<Livro>();
   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Livro> getLivros() {
        return livros;
    }

    public void setLivros(List<Livro> livros) {
        this.livros = livros;
    }

   
   
}
