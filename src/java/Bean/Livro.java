/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bean;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

/**
 *
 * @author ElaineSantos
 */

@Entity(name="livro")
public class Livro {
    
    @Id //define que o atributo isbn será a chave da tabela
    @Column(name="isbn", nullable=false) //como não é autoincremento, não pode ser nulo
    private String isbn;
    
    @Column(name="titulo", nullable=false, columnDefinition="VARCHAR(100)", length=100)
    private String titulo;
    
    @Column(name="edicao", columnDefinition="int", length=10)
    private int edicao;
    
    @ManyToMany(mappedBy="Livro")
    private List<Autor> autores = new ArrayList<Autor>();

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getEdicao() {
        return edicao;
    }

    public void setEdicao(int edicao) {
        this.edicao = edicao;
    }

    public List<Autor> getAutores() {
        return autores;
    }

    public void setAutores(List<Autor> autores) {
        this.autores = autores;
    }

       
    
    
}
