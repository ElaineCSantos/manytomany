/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Bean.Livro;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import util.HibernateUtil;

/**
 *
 * @author ElaineSantos
 */
public class DaoLivro {
    static SessionFactory fabrica = HibernateUtil.getSessionFactory();
    public void adicionarLivro(List autores, String titulo, int edicao, String isbn){
        Livro livro = new Livro();
        
        livro.setAutores(autores);
        livro.setTitulo(titulo);
        livro.setEdicao(edicao);
        livro.setIsbn(isbn);
        
        Session session = fabrica.openSession();
        session.beginTransaction();
        session.save(livro);
        session.getTransaction().commit();
        session.close();        
    }
    
    public void excluirLivro(Livro livro){
        Session session = fabrica.openSession();
        session.beginTransaction();
        livro.getAutores().clear();
        session.delete(livro);      
        session.getTransaction().commit();
        session.close(); 
    }
    
    public Livro buscaLivro(String titulo) {
        Session session = fabrica.openSession();
        Query q = session.createQuery("from Livro where nome= :titulo");
        return (Livro) q.setString("titulo", titulo).uniqueResult();
    }
    
    public Livro retornarLivro(String isbn) {
        Session session = fabrica.openSession();
        Query q = session.createQuery("from Livro where isbn = :isbn");
        return (Livro) q.setString("isbn", isbn).uniqueResult();
    }
    
    public void atualizarLivro(Livro livro) {
        Session session = fabrica.openSession();
        session.beginTransaction();
        session.update(livro);
        session.getTransaction().commit();
        session.close();
    }
    
    public List<Livro> listarLivros() {
        Session session = fabrica.openSession();
        Query q = session.createQuery("from Livro");
        List lista = q.list();
        return lista;
    }
    
    
}
